module.exports = {
  content: [
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      colors: {
        primary: '#FFD500',
        secondary: '#005BBB',
        tertiary: '#ff0000'
      }
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}
