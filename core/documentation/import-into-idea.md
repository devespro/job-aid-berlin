# Import into IntelliJ IDEA

The following has been tested against IntelliJ IDEA 2022.1

## Steps

_Within your locally cloned JobAid Core Service working directory:_

1. Import into IntelliJ (File -> New -> Project from Existing Sources -> Navigate to directory -> Select build.gradle)
1. Happy coding! :tada:

## Tips

In any case, please do not check in your own generated .iml, .ipr or .iws files in git.
You'll notice these files are already intentionally in .gitignore. The same policy goes for eclipse metadata.
