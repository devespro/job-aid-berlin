package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Badword;
import com.jobaidukraine.core.services.ports.in.queries.BadwordQuery;
import com.jobaidukraine.core.services.ports.in.usecases.BadwordAdministrationUsecase;
import com.jobaidukraine.core.services.ports.in.usecases.BadwordCheckUseCase;
import com.jobaidukraine.core.services.ports.out.BadwordPort;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BadwordService
    implements BadwordAdministrationUsecase, BadwordCheckUseCase, BadwordQuery {

  private final BadwordPort badwordPort;

  @Override
  public List<Badword> findAll() {
    return badwordPort.findAll();
  }

  @Override
  @Transactional
  public List<Badword> overwriteBadwords(List<Badword> badwords) {
    badwordPort.deleteAll();
    return this.badwordPort.save(badwords);
  }

  @Override
  public List<String> check(String textToCheck) {
    if (StringUtils.isEmpty(textToCheck)) {
      return Collections.emptyList();
    }
    String smallText = textToCheck.toLowerCase();
    List<Badword> badwords = badwordPort.findAll();
    return badwords.stream()
        .map(Badword::getWord)
        .filter(badword -> smallText.contains(badword.toLowerCase()))
        .distinct()
        .collect(Collectors.toList());
  }
}
