package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Badword;
import java.util.List;

public interface BadwordAdministrationUsecase {
  List<Badword> overwriteBadwords(List<Badword> badwords);
}
