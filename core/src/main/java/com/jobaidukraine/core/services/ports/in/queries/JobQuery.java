package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JobQuery {
  Job findById(long id);

  Page<Job> findAllByPageable(Pageable pageable);

  Page<Job> findAllByPageable(Pageable pageable, String title);

  Page<Job> findAllFlaggedByPageable(Pageable pageable);
}
