package com.jobaidukraine.core.services.ports.in.usecases;

import java.util.List;

public interface BadwordCheckUseCase {
  /** @return the list of mateched badwords, empty if not badwords were found */
  List<String> check(String textToCheck);
}
