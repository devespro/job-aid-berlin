package com.jobaidukraine.core.services.ports.in.queries;

import com.jobaidukraine.core.domain.Badword;
import java.util.List;

public interface BadwordQuery {
  List<Badword> findAll();
}
