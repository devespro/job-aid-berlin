package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.Badword;
import java.util.List;

public interface BadwordPort {
  List<Badword> save(List<Badword> badword);

  List<Badword> findAll();

  void deleteAll();
}
