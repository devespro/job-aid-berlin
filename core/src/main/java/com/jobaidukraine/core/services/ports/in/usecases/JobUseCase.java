package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Job;

public interface JobUseCase {
  Job save(Job job, Long companyId);

  Job update(Job job);

  void delete(long id);

  void approve(long id);

  void decline(long id);
}
