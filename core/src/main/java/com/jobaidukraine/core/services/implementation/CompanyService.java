package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.services.ports.in.queries.CompanyQuery;
import com.jobaidukraine.core.services.ports.in.usecases.BadwordCheckUseCase;
import com.jobaidukraine.core.services.ports.in.usecases.CompanyUseCase;
import com.jobaidukraine.core.services.ports.out.CompanyPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CompanyService implements CompanyUseCase, CompanyQuery {

  private final CompanyPort companyPort;
  private final BadwordCheckUseCase badwordCheckUseCase;

  @Override
  public Company save(Company company) {
    var jobs = company.getJobs();
    jobs.forEach(this::checkForBadWords);
    return this.companyPort.save(company);
  }

  private void checkForBadWords(Job job) {
    job.badWordsCheckCompleted(badwordCheckUseCase.check(job.getSensitiveContent()));
  }

  @Override
  public Page<Company> findAllByPageable(Pageable pageable) {
    return companyPort.findAllByPageable(pageable);
  }

  @Override
  public Company findById(long id) {
    return this.companyPort.findById(id).orElseThrow();
  }

  @Override
  public Company update(Company company) {
    return this.companyPort.update(company);
  }

  @Override
  public void delete(long id) {
    companyPort.delete(id);
  }
}
