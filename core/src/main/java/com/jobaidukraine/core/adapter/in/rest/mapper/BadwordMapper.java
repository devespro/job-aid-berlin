package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.BadwordDto;
import com.jobaidukraine.core.adapter.out.db.entities.BadwordEntity;
import com.jobaidukraine.core.domain.Badword;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BadwordMapper {
  @Mapping(target = "id", ignore = true)
  BadwordEntity domainToEntity(Badword badword);

  List<Badword> dtoListToDomainList(List<BadwordDto> badwords);

  List<BadwordDto> domainListToDtoList(List<Badword> badwords);

  List<Badword> entityListToDomainList(List<BadwordEntity> badwords);

  List<BadwordEntity> domainListToEntityList(List<Badword> badwords);
}
