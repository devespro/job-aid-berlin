package com.jobaidukraine.core.adapter.out.db.implementation.job;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.JobMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.JobState;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobAdapter implements JobPort {

  private final JobRepository jobRepository;
  private final JobMapper jobMapper;

  @Override
  public Optional<Job> findById(long id) {
    return jobRepository.findById(id).map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable) {
    return jobRepository
        .findByStateNotIn(pageable, JobState.getHiddenStates())
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable, String title) {
    return jobRepository
        .findByStateNotInAndTitleContaining(pageable, JobState.getHiddenStates(), title)
        .map(jobMapper::toDomain);
  }

  @Override
  public Page<Job> findAllFlaggedByPageable(Pageable pageable) {
    return jobRepository.findByState(pageable, JobState.FLAGGED.name()).map(jobMapper::toDomain);
  }

  @Override
  public Job update(Job job) {
    JobEntity jobEntity = jobRepository.findById(job.getId()).orElseThrow();

    if (job.getTitle() != null) {
      jobEntity.setTitle(job.getTitle());
    }

    if (job.getDescription() != null) {
      jobEntity.setDescription(job.getDescription());
    }

    if (job.getJobType() != null) {
      jobEntity.setJobType(job.getJobType().name());
    }

    if (job.getState() != null) {
      jobEntity.setState(job.getState().name());
    }

    if (job.getBadWordState() != null) {
      jobEntity.setBadWordState(job.getBadWordState().name());
    }

    return jobMapper.toDomain(jobRepository.save(jobEntity));
  }

  @Override
  public void delete(long id) {
    JobEntity job = jobRepository.findById(id).orElseThrow();
    job.setDeleted(true);
    jobRepository.save(job);
  }
}
