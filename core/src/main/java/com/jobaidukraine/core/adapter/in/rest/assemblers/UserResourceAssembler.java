package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.UserController;
import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserResourceAssembler
    implements RepresentationModelAssembler<UserDto, EntityModel<UserDto>> {

  @Override
  public EntityModel<UserDto> toModel(UserDto user) {

    try {
      return EntityModel.of(
          user,
          linkTo(methodOn(UserController.class).show(user.getId())).withSelfRel(),
          linkTo(methodOn(UserController.class).list(null)).withRel("users"));

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return null;
  }
}
