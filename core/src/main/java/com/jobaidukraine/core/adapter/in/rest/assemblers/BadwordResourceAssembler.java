package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.BadwordController;
import com.jobaidukraine.core.adapter.in.rest.dto.BadwordDto;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BadwordResourceAssembler
    implements RepresentationModelAssembler<List<BadwordDto>, CollectionModel<BadwordDto>> {

  @Override
  public CollectionModel<BadwordDto> toModel(List<BadwordDto> badwords) {
    return CollectionModel.of(
        badwords, linkTo(methodOn(BadwordController.class).list()).withRel("self"));
  }
}
