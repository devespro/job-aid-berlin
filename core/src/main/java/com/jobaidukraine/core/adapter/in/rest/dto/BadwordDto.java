package com.jobaidukraine.core.adapter.in.rest.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Badword", description = "A badword that should flag jobs for manual approval")
public class BadwordDto {
  @NotNull(message = "The word should not be Null")
  @NotBlank(message = "The word may not be blank or empty.")
  @Schema(description = "The String that is the Badword", required = true, example = "Scheisse")
  private String word;
}
