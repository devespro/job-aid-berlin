package com.jobaidukraine.core.adapter.out.db.entities;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import javax.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "id",
    scope = AddressEntity.class)
public class AddressEntity {

  private Float latitude;
  private Float longitude;
  private String street;
  private String houseNumber;
  private String zip;
  private String city;
  private String country;
}
