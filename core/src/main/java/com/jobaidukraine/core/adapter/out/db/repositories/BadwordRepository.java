package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.BadwordEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BadwordRepository extends JpaRepository<BadwordEntity, Long> {}
