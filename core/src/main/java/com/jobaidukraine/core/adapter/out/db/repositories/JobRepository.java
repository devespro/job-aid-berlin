package com.jobaidukraine.core.adapter.out.db.repositories;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JobRepository extends JpaRepository<JobEntity, Long> {

  Page<JobEntity> findByStateNotIn(Pageable pageable, Set<String> states);

  Page<JobEntity> findByStateNotInAndTitleContaining(
      Pageable pageable, Set<String> states, String title);

  Page<JobEntity> findByState(Pageable pageable, String state);
}
