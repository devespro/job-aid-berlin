package com.jobaidukraine.core.adapter.out.db.implementation.badword;

import com.jobaidukraine.core.adapter.in.rest.mapper.BadwordMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.BadwordRepository;
import com.jobaidukraine.core.domain.Badword;
import com.jobaidukraine.core.services.ports.out.BadwordPort;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BadwordAdapter implements BadwordPort {

  private final BadwordRepository badwordRepository;
  private final BadwordMapper badwordMapper;

  @Override
  public List<Badword> findAll() {
    return badwordMapper.entityListToDomainList(badwordRepository.findAll());
  }

  @Override
  public List<Badword> save(List<Badword> badwords) {
    return badwordMapper.entityListToDomainList(
        badwordRepository.saveAll(badwordMapper.domainListToEntityList(badwords)));
  }

  @Override
  public void deleteAll() {
    badwordRepository.deleteAll();
  }
}
