package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.BadwordResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.BadwordDto;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.BadwordMapper;
import com.jobaidukraine.core.services.ports.in.queries.BadwordQuery;
import com.jobaidukraine.core.services.ports.in.usecases.BadwordAdministrationUsecase;
import com.jobaidukraine.core.services.ports.in.usecases.BadwordCheckUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.CollectionModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/badwords")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "Badword")})
public class BadwordController {

  private final BadwordResourceAssembler resourceAssembler;

  private final BadwordAdministrationUsecase badwordAdministrationUsecase;

  private final BadwordCheckUseCase badwordCheckUseCase;

  private final BadwordMapper badwordMapper;

  private final BadwordQuery badwordQuery;

  @Operation(
      summary = "Get badword list",
      tags = {"Badword"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful retrieval of all badwords",
            content = {
              @Content(
                  mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = CompanyDto.class)))
            }),
        @ApiResponse(responseCode = "401", description = "Unauthorized")
      })
  @GetMapping
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public CollectionModel<BadwordDto> list() {
    return resourceAssembler.toModel(badwordMapper.domainListToDtoList(badwordQuery.findAll()));
  }

  @Operation(
      summary = "Overwrite badword list",
      tags = {"Badword"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful overwrite of badword list",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = CompanyDto.class))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid badword list data"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PostMapping
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public CollectionModel<BadwordDto> save(@RequestBody List<BadwordDto> badwords) {

    List<BadwordDto> savedBadwords =
        badwordMapper.domainListToDtoList(
            this.badwordAdministrationUsecase.overwriteBadwords(
                badwordMapper.dtoListToDomainList(badwords)));
    return resourceAssembler.toModel(savedBadwords);
  }

  @Operation(
      summary = "Preview check for Badwords in the Text",
      tags = {"Badword"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful check for Badwords",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = String.class))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid text"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PostMapping("/check")
  @PreAuthorize("hasAuthority('ROLE_ADMIN')")
  public CollectionModel<String> check(@RequestBody String text) {
    return CollectionModel.of(this.badwordCheckUseCase.check(text));
  }
}
