package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Job {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;

  private String title;
  private String description;
  private JobType jobType;
  private String applicationLink;
  private String applicationMail;
  private String videoUrl;

  private Address address;
  private Set<Experience> qualifications;
  private Classification classification;
  private Set<LanguageLevel> languageLevels;

  private JobState state = JobState.DISABLED;
  private BadWordState badWordState = BadWordState.UNCHECKED;

  public void badWordsCheckCompleted(List<String> result) {
    if (result.isEmpty()) {
      this.badWordState = BadWordState.NEGATIVE;
      this.state = JobState.APPROVED;
    } else {
      this.badWordState = BadWordState.POSITIVE;
      this.state = JobState.FLAGGED;
    }
  }

  public void decline() {
    this.setDeleted(true);
    this.setState(JobState.DELETED);
    this.setBadWordState(BadWordState.POSITIVE);
  }

  public void approve() {
    this.setState(JobState.APPROVED);
    this.setBadWordState(BadWordState.FALSE_POSITIVE);
  }

  public String getSensitiveContent() {
    return String.join(" | ", title, description);
  }
}
