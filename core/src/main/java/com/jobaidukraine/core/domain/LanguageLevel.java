package com.jobaidukraine.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LanguageLevel {
  private String language;
  private LanguageSkill languageSkill;
}
