package com.jobaidukraine.core.domain;

public enum BadWordState {
  UNCHECKED,
  NEGATIVE,
  POSITIVE,
  FALSE_POSITIVE;
}
