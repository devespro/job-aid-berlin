package com.jobaidukraine.core.domain;

public enum LanguageSkill {
  NATIVE("NATIVE"),
  FLUENT("FLUENT"),
  GOOD("GOOD"),
  BASIC("BASIC");

  final String skill;

  LanguageSkill(String skill) {
    this.skill = skill;
  }

  public String getSkill() {
    return this.skill;
  }
}
