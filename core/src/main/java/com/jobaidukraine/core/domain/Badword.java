package com.jobaidukraine.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Badword {
  private String word;
}
