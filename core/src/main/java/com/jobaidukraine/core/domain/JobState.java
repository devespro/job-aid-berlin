package com.jobaidukraine.core.domain;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum JobState {
  FLAGGED,
  APPROVED,
  DELETED,
  DISABLED;

  public static Set<String> getHiddenStates() {
    return Stream.of(FLAGGED, DELETED, DISABLED).map(Enum::name).collect(Collectors.toSet());
  }
}
