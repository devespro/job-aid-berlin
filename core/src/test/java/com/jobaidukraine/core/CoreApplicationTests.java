package com.jobaidukraine.core;

import static org.assertj.core.api.Assertions.assertThat;

import com.jobaidukraine.core.adapter.in.rest.dto.AddressDto;
import com.jobaidukraine.core.adapter.in.rest.dto.BadwordDto;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDtoTestDataFactory;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.dto.LanguageLevelDto;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("local")
class CoreApplicationTests {

  @LocalServerPort private int localServerPort;

  @Autowired private RestTemplateBuilder restTemplateBuilder;

  private TestRestTemplate template;

  @PostConstruct
  public void initialize() {
    RestTemplateBuilder customTemplate =
        restTemplateBuilder.rootUri("http://localhost:" + localServerPort);
    this.template = new TestRestTemplate(customTemplate);
  }

  @Test
  void healthIsUp() {
    assertThat(template.getForObject("/actuator/health", String.class)).contains("UP");
  }

  @Test
  void postedJobWithoutBadWordsWillBeInstantlyApproved() {
    CompanyDto company = getNewCompanyWithJob("test description");
    checkJobStateAfterSave(company, "APPROVED");
  }

  @Test
  void postedJobWithoutBadWordsWillBeFlagged() {
    var offensiveContent = "offensive content";
    saveNewBadWord(offensiveContent);

    CompanyDto company = getNewCompanyWithJob(offensiveContent);
    checkJobStateAfterSave(company, "FLAGGED");
  }

  private void saveNewBadWord(String badWord) {
    var actual =
        template.postForEntity(
            "/v1.0/badwords", List.of(new BadwordDto(badWord)), BadwordDto.class);
    checkStatusOk(actual);
  }

  private void checkJobStateAfterSave(CompanyDto company, String stateName) {
    HttpEntity<CompanyDto> request = new HttpEntity<>(company);
    var actual = template.postForEntity("/v1.0/companies", request, CompanyDto.class);
    var dto = checkStatusOk(actual);
    assertThat(dto.getJobs()).extracting(JobDto::getState).containsExactly(stateName);
  }

  private <T> T checkStatusOk(ResponseEntity<T> actual) {
    assertThat(actual)
        .isNotNull()
        .extracting(ResponseEntity::getStatusCode)
        .isEqualTo(HttpStatus.OK);
    assertThat(actual.getBody()).isNotNull();
    return actual.getBody();
  }

  private CompanyDto getNewCompanyWithJob(String jobDescription) {

    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String jobType = "FREELANCE";
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    AddressDto address = new AddressDto();
    Set<String> qualifications = new HashSet<>();
    Set<LanguageLevelDto> languageLevels = new HashSet<>();
    String state = null;
    JobDto newJob =
        new JobDto(
            id,
            createdAt,
            updatedAt,
            version,
            deleted,
            title,
            jobDescription,
            jobType,
            applicationLink,
            applicationMail,
            videoUrl,
            address,
            qualifications,
            null,
            languageLevels,
            state);
    newJob.setCreatedAt(null);
    newJob.setUpdatedAt(null);
    newJob.setVersion(null);

    CompanyDto company = CompanyDtoTestDataFactory.getMockCompany();
    company.setCreatedAt(null);
    company.setUpdatedAt(null);
    company.setVersion(null);

    company.getJobs().clear();
    company.getJobs().add(newJob);
    return company;
  }
}
