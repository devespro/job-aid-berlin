package com.jobaidukraine.core.adapter.in.rest.dto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class JobDtoTestDataFactory {

  public static JobDto getMockJob() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    String jobType = "FREELANCE";
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    AddressDto address = new AddressDto();
    Set<String> qualifications = new HashSet<>();
    Set<LanguageLevelDto> languageLevels = new HashSet<>();
    String state = null;
    return new JobDto(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        null,
        languageLevels,
        state);
  }
}
