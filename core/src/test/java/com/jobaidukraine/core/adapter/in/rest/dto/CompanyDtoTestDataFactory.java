package com.jobaidukraine.core.adapter.in.rest.dto;

import static org.junit.jupiter.api.Assertions.*;

import com.jobaidukraine.core.domain.User;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class CompanyDtoTestDataFactory {

  public static CompanyDto getMockCompany() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<JobDto> jobs = new HashSet<>();
    AddressDto address = new AddressDto();
    User contactUser = new User();
    return new CompanyDto(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }
}
