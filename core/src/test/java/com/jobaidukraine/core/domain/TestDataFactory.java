package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class TestDataFactory {

  public static Company getMockCompany() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<Job> jobs = new HashSet<>();
    Address address = new Address();
    User contactUser = new User();
    return new Company(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }

  public static Job getMockJob() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String title = "test title";
    String description = "test description";
    JobType jobType = JobType.FREELANCE;
    String applicationLink = "test-applicationlink.test";
    String applicationMail = "test@test.test";
    String videoUrl = "test-videourl.test";
    Address address = new Address();
    Set<Experience> qualifications = new HashSet<>();
    Set<LanguageLevel> languageLevels = new HashSet<>();
    return new Job(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        title,
        description,
        jobType,
        applicationLink,
        applicationMail,
        videoUrl,
        address,
        qualifications,
        null,
        languageLevels,
        JobState.DISABLED,
        BadWordState.UNCHECKED);
  }
}
