package com.jobaidukraine.core.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

class JobTest {

  @Test
  void newJobsShouldBeDisabledAndUnchecked() {
    var job = new Job();
    assertThat(job.getState()).isEqualTo(JobState.DISABLED);
    assertThat(job.getBadWordState()).isEqualTo(BadWordState.UNCHECKED);
  }

  @Test
  void postedJobWithBadWordsShouldBeFlagged() {

    var job = new Job();

    job.badWordsCheckCompleted(List.of("OFFENSIVE WORD"));

    assertThat(job.getBadWordState()).isEqualTo(BadWordState.POSITIVE);
    assertThat(job.getState()).isEqualTo(JobState.FLAGGED);
  }

  @Test
  void postedJobWithoutBadWordsShouldBeApproved() {
    var job = new Job();

    job.badWordsCheckCompleted(List.of());

    assertThat(job.getBadWordState()).isEqualTo(BadWordState.NEGATIVE);
    assertThat(job.getState()).isEqualTo(JobState.APPROVED);
  }
}
