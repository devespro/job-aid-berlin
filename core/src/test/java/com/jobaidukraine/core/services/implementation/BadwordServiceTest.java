package com.jobaidukraine.core.services.implementation;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.Badword;
import com.jobaidukraine.core.services.ports.out.BadwordPort;
import java.util.Collections;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BadwordServiceTest {

  @Mock BadwordPort badwordPort;

  @InjectMocks private BadwordService badwordService;

  @Test
  void noBadWordsShouldAcceptAnyText() {
    // given
    when(badwordPort.findAll()).thenReturn(Collections.emptyList());
    String text = "Heute ist es Scheisse.";
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).isEmpty();
  }

  @Test
  void badWordsConfiguredButNoMatchShouldAcceptText() {
    // given
    when(badwordPort.findAll()).thenReturn(List.of(new Badword("Scheisse")));
    String text = "Heute ist es schön.";
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).isEmpty();
  }

  @Test
  void badWordsConfiguredMatchShouldReturnBadWordMatches() {
    // given
    when(badwordPort.findAll()).thenReturn(List.of(new Badword("Scheisse")));
    String text = "Heute ist es scheisse.";
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).containsExactly("Scheisse");
  }

  @Test
  void findMultipleMatches() {
    // given
    when(badwordPort.findAll())
        .thenReturn(List.of(new Badword("Scheisse"), new Badword("Blöd"), new Badword("Blöd")));
    String text = "Heute ist es scheisse und Blöd.";
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).containsExactly("Scheisse", "Blöd");
  }

  @Test
  void badWordswithUmlautsMatchShouldReturnBadWordMatch() {
    // given
    when(badwordPort.findAll()).thenReturn(List.of(new Badword("лайно")));
    String text = "Heute ist es ЛАЙНО.";
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).containsExactly("лайно");
  }

  @Test
  void shouldDealWithNullText() {
    // given
    String text = null;
    // when
    List<String> returnedBadwords = badwordService.check(text);
    // then
    assertThat(returnedBadwords).isEmpty();
  }
}
