module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '3ae2363c904a86cc460d5b7f06c5cadf'),
  },
});
